package lt.mazgis.homework.telia.lb.fxrates.ext;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CcyNm")
public class CurrencyName {

    @XmlAttribute(name="lang")
    private String lang;

    @XmlValue
    private String name;

    public CurrencyName() {
    }

    public CurrencyName(final String lang, final String name) {
	super();
	this.lang = lang;
	this.name = name;
    }

    public String getLang() {
	return this.lang;
    }
    public String getName() {
	return this.name;
    }

}
