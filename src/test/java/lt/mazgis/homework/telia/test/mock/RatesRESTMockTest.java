package lt.mazgis.homework.telia.test.mock;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import lt.mazgis.homework.telia.lb.LBFxRatesClient;
import lt.mazgis.homework.telia.lb.fxrates.ext.CurrencyAmount;
import lt.mazgis.homework.telia.lb.fxrates.ext.CurrencyName;
import lt.mazgis.homework.telia.lb.fxrates.ext.CurrencyType;
import lt.mazgis.homework.telia.lb.fxrates.ext.FxRate;
import lt.mazgis.homework.telia.lb.fxrates.ext.FxRatesResponce;
import lt.mazgis.homework.telia.rest.CurrencyRatesREST;
import lt.mazgis.homework.telia.rest.dao.CurrencyNameDto;
import lt.mazgis.homework.telia.rest.dao.CurrencyRateDto;

@RunWith(MockitoJUnitRunner.class)
public class RatesRESTMockTest {

    @Mock
    LBFxRatesClient ratesClient;

    @InjectMocks
    CurrencyRatesREST service;

    @Test
    public void testDataConvertion() {
	when(this.ratesClient.getCurrencyList()).thenReturn(this.getCurrencyType());
	when(this.ratesClient.getCurrentFxRates(argThat(m -> m.equals("EU")))).thenReturn(this.getFxRateValue());
	final Collection<CurrencyRateDto> responce = this.service.rates();
	verify(this.ratesClient, times(1)).getCurrencyList();
	verify(this.ratesClient, times(1)).getCurrentFxRates(argThat(m -> m.equals("EU")));
	assertNotNull(responce);
	assertTrue(responce.size() == 1);
	responce.forEach(r -> {
	    assertNotNull(r);
	    assertTrue(r.getAcronym().equals("MDL"));
	    assertTrue(r.getDate().equals("2018-11-03"));
	    assertTrue(r.getRate().equals("19.548050"));
	    assertNotNull(r.getNames());
	    assertTrue(r.getNames().size() == 2);
	    final CurrencyNameDto[] names = r.getNames().toArray(new CurrencyNameDto[2]);
	    assertNotNull(names[0]);
	    assertNotNull(names[1]);
	    assertFalse(names[0].getLang().equals(names[1].getLang()));
	    assertFalse(names[0].getName().equals(names[1].getName()));
	    assertTrue(names[0].getLang().endsWith("LT") || names[0].getLang().endsWith("EN"));
	    assertTrue(names[1].getLang().endsWith("LT") || names[1].getLang().endsWith("EN"));
	    assertTrue(names[0].getName().endsWith("Moldovos lėja") || names[0].getName().endsWith("Moldovan leu"));
	    assertTrue(names[1].getName().endsWith("Moldovos lėja") || names[1].getName().endsWith("Moldovan leu"));

	});

    }

    private FxRatesResponce getCurrencyType() {
	final FxRatesResponce responce = new FxRatesResponce();
	responce.setCurrenctyTypes(Arrays.asList(
		new CurrencyType("MDL", "498", "2", Arrays.asList(new CurrencyName("LT", "Moldovos lėja"), new CurrencyName("EN", "Moldovan leu")))));
	return responce;
    }

    private FxRatesResponce getFxRateValue() {
	final FxRatesResponce responce = new FxRatesResponce();
	responce.setFxRates(
		Arrays.asList(new FxRate("EU", "2018-11-03", Arrays.asList(new CurrencyAmount("EUR", "1"), new CurrencyAmount("MDL", "19.548050")))));
	return responce;
    }

}

