package lt.mazgis.homework.telia.lb.fxrates.ext;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CcyNtry")
public class CurrencyType {

    @XmlElement(name="Ccy")
    private String acronym;

    @XmlElement(type=CurrencyName.class,name="CcyNm")
    private Collection<CurrencyName> names;

    @XmlElement(name="CcyNbr")
    private String nr;

    @XmlElement(name="CcyMnrUnts")
    private String mnrUnts;

    public CurrencyType() {
    }

    public CurrencyType(final String acronym, final String nr, final String mnrUnts, final Collection<CurrencyName> names) {
	super();
	this.acronym = acronym;
	this.names = names;
	this.nr = nr;
	this.mnrUnts = mnrUnts;
    }

    public String getAcronym() {
	return this.acronym;
    }
    public String getMnrUnts() {
	return this.mnrUnts;
    }
    public Collection<CurrencyName> getNames() {
	return this.names;
    }
    public String getNr() {
	return this.nr;
    }

}
