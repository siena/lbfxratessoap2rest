package lt.mazgis.homework.telia.lb.fxrates.ext;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "FxRates")
public class FxRatesResponce {

    @XmlElement(type=FxRate.class,name="FxRate")
    private Collection<FxRate> fxRates;

    @XmlElement(type=CurrencyType.class,name="CcyNtry")
    private Collection<CurrencyType> currenctyTypes;

    public Collection<FxRate> getFxRates() {
	return this.fxRates;
    }

    public void setFxRates(final Collection<FxRate> fxRates) {
	this.fxRates = fxRates;
    }

    public Collection<CurrencyType> getCurrenctyTypes() {
	return this.currenctyTypes;
    }

    public void setCurrenctyTypes(final Collection<CurrencyType> currenctyTypes) {
	this.currenctyTypes = currenctyTypes;
    }
}
