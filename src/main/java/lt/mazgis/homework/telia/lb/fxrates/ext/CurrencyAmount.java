package lt.mazgis.homework.telia.lb.fxrates.ext;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CcyAmt")
public class CurrencyAmount {
    @XmlElement(name = "Ccy")
    private String currency;
    @XmlElement(name = "Amt")
    private String amount;

    public CurrencyAmount() {
    }

    public CurrencyAmount(final String currency, final String amount) {
	super();
	this.currency = currency;
	this.amount = amount;
    }

    public String getAmount() {
	return this.amount;
    }

    public String getCurrency() {
	return this.currency;
    }

}
