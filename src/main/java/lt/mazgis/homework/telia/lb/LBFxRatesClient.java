package lt.mazgis.homework.telia.lb;

import java.net.URI;

import org.springframework.util.Assert;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import lt.mazgis.homework.telia.lb.fxrates.GetCurrencyList;
import lt.mazgis.homework.telia.lb.fxrates.GetCurrencyListResponse;
import lt.mazgis.homework.telia.lb.fxrates.GetCurrentFxRates;
import lt.mazgis.homework.telia.lb.fxrates.GetCurrentFxRatesResponse;
import lt.mazgis.homework.telia.lb.fxrates.GetFxRates;
import lt.mazgis.homework.telia.lb.fxrates.GetFxRatesResponse;
import lt.mazgis.homework.telia.lb.fxrates.ext.FxRatesResponce;

public class LBFxRatesClient extends WebServiceGatewaySupport {

    public LBFxRatesClient(final String uri) throws Exception {
	try {
	    URI.create(uri);
	} catch (final IllegalArgumentException e) {
	    throw new Exception("Incorrct URI provided : " + uri, e);
	}
	this.setDefaultUri(uri);
    }

    public FxRatesResponce getCurrencyList() {
	final GetCurrencyList request = new GetCurrencyList();
	final GetCurrencyListResponse response = (GetCurrencyListResponse) this.getWebServiceTemplate().marshalSendAndReceive(request,new SoapActionCallback("http://www.lb.lt/WebServices/FxRates/getCurrencyList"));
	Assert.notNull(response.getGetCurrencyListResult(), "getCurrencyList responce has not value");
	Assert.notNull(response.getGetCurrencyListResult().getContent(), "getCurrencyList responce value haz no content");
	Assert.notEmpty(response.getGetCurrencyListResult().getContent(), "getCurrencyList responce value content is empty");
	Assert.isInstanceOf(FxRatesResponce.class, response.getGetCurrencyListResult().getContent().get(0), "getCurrencyList responce value content incorrect type");
	return (FxRatesResponce)response.getGetCurrencyListResult().getContent().get(0);
    }

    public GetFxRatesResponse getFxRates(final String tp, final String dt) {
	final GetFxRates request = new GetFxRates();
	request.setTp(tp);
	request.setDt(dt);
	final GetFxRatesResponse response = (GetFxRatesResponse) this.getWebServiceTemplate().marshalSendAndReceive(request,new SoapActionCallback("http://www.lb.lt/WebServices/FxRates/getFxRates"));
	return response;
    }

    public FxRatesResponce getCurrentFxRates(final String tp) {
	final GetCurrentFxRates request = new GetCurrentFxRates();
	request.setTp(tp);
	final GetCurrentFxRatesResponse response = (GetCurrentFxRatesResponse) this.getWebServiceTemplate()
		.marshalSendAndReceive(request,new SoapActionCallback("http://www.lb.lt/WebServices/FxRates/getCurrentFxRates"));
	Assert.notNull(response.getGetCurrentFxRatesResult(), "getCurrentFxRates responce has not value");
	Assert.notNull(response.getGetCurrentFxRatesResult().getContent(), "getCurrentFxRates responce value haz no content");
	Assert.notEmpty(response.getGetCurrentFxRatesResult().getContent(), "getCurrentFxRates responce value content is empty");
	Assert.isInstanceOf(FxRatesResponce.class, response.getGetCurrentFxRatesResult().getContent().get(0), "getCurrentFxRates responce value content incorrect type");
	return (FxRatesResponce)response.getGetCurrentFxRatesResult().getContent().get(0);

    }

}
