package lt.mazgis.homework.telia.rest.dao;


public class CurrencyNameDto {
    private String lang;
    private String name;

    public CurrencyNameDto() {
    }

    public CurrencyNameDto(final String lang, final String name) {
	this.lang = lang;
	this.name = name;
    }

    public String getLang() {
	return this.lang;
    }

    public String getName() {
	return this.name;
    }

}
