package lt.mazgis.homework.telia.test;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import lt.mazgis.homework.telia.ServiceConverterConfigurator;
import lt.mazgis.homework.telia.lb.LBFxRatesClient;
import lt.mazgis.homework.telia.lb.fxrates.ext.CurrencyAmount;
import lt.mazgis.homework.telia.lb.fxrates.ext.CurrencyName;
import lt.mazgis.homework.telia.lb.fxrates.ext.CurrencyType;
import lt.mazgis.homework.telia.lb.fxrates.ext.FxRate;
import lt.mazgis.homework.telia.lb.fxrates.ext.FxRatesResponce;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { ServiceConverterConfigurator.class })
@TestPropertySource(locations = "classpath:test-config.properties")
public class LBFxRatesSoapMethosTests {

    @Autowired(required = true)
    private LBFxRatesClient ratesClient;

    @Test
    public void testGetCurrentFxRates() throws JAXBException {
	final FxRatesResponce response = this.ratesClient.getCurrentFxRates("EU");
	assertNotNull(response);
	assertNotNull(response.getFxRates());
	assertTrue(response.getFxRates().size() > 0);
	for (final FxRate r : response.getFxRates()) {
	    assertNotNull(r.getType());
	    assertNotNull(r.getDate());
	    assertNotNull(r.getCurrancyAmounts());
	    assertTrue(r.getCurrancyAmounts().size() == 2);
	    for (final CurrencyAmount ca : r.getCurrancyAmounts()) {
		assertNotNull(ca.getAmount());
		assertNotNull(ca.getCurrency());
	    }
	}
    }

    @Test
    public void testGetCurrencyList() {
	final FxRatesResponce response = this.ratesClient.getCurrencyList();
	assertNotNull(response);
	assertNotNull(response.getCurrenctyTypes());
	assertTrue(response.getCurrenctyTypes().size() > 0);
	for (final CurrencyType t : response.getCurrenctyTypes()) {
	    assertNotNull(t.getAcronym());
	    assertNotNull(t.getMnrUnts());
	    assertNotNull(t.getNr());
	    assertNotNull(t.getNames());
	    assertTrue(t.getNames().size() == 2);
	    for (final CurrencyName ca : t.getNames()) {
		assertNotNull(ca.getLang());
		assertNotNull(ca.getName());
	    }
	}
    }
}
