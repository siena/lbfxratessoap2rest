package lt.mazgis.homework.telia.rest.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import lt.mazgis.homework.telia.lb.fxrates.ext.CurrencyAmount;
import lt.mazgis.homework.telia.lb.fxrates.ext.CurrencyType;
import lt.mazgis.homework.telia.lb.fxrates.ext.FxRate;

public class CurrencyRateDto {

    private String acronym;
    private Collection<CurrencyNameDto> names;
    private String date;
    private String rate;

    public String getAcronym() {
	return this.acronym;
    }

    public String getDate() {
	return this.date;
    }

    public Collection<CurrencyNameDto> getNames() {
	return this.names;
    }

    public String getRate() {
	return this.rate;
    }

    public static CurrencyRateDto create(final FxRate rate, final Map<String, CurrencyType> types) {
	final CurrencyRateDto result = new CurrencyRateDto();


	result.date = rate.getDate();
	final Optional<CurrencyAmount> amount = rate.getCurrancyAmounts().stream()
		.filter(a -> !a.getCurrency().contentEquals("EUR")).findFirst();

	result.rate = amount.get().getAmount();
	result.acronym = amount.get().getCurrency();
	final CurrencyType type = types.get(amount.get().getCurrency());
	if (type != null) {
	    result.names = new ArrayList<>(type.getNames().size());
	    type.getNames().forEach(n -> result.names.add(new CurrencyNameDto(n.getLang(), n.getName())));
	}
	return result;
    }
}
