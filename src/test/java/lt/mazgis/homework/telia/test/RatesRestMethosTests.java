package lt.mazgis.homework.telia.test;

import static org.junit.Assert.*;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import lt.mazgis.homework.telia.ServiceConverterConfigurator;
import lt.mazgis.homework.telia.rest.dao.CurrencyNameDto;
import lt.mazgis.homework.telia.rest.dao.CurrencyRateDto;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { ServiceConverterConfigurator.class })
@TestPropertySource(locations = "classpath:test-config.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RatesRestMethosTests {

    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
	this.base = new URL("http://localhost:" + this.port + "/rates");
    }

    @Test
    public void testService() {
	final ResponseEntity<CurrencyRateDto[]> response = this.template.getForEntity(this.base.toString(), CurrencyRateDto[].class);
	assertNotNull(response);
	assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	assertNotNull(response.getBody());
	assertTrue(response.getBody().length > 0);
	for (final CurrencyRateDto cr : response.getBody()) {
	    assertNotNull(cr.getAcronym());
	    assertNotNull(cr.getDate());
	    assertNotNull(cr.getRate());
	    assertNotNull(cr.getNames());
	    assertTrue(cr.getNames().size() == 2);
	    for (final CurrencyNameDto name : cr.getNames()) {
		assertNotNull(name.getLang());
		assertNotNull(name.getName());
		assertTrue(name.getLang().equals("LT") || name.getLang().equals("EN"));
	    }
	}
    }

}
