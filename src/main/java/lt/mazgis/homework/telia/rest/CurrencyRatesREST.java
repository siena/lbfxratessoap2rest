package lt.mazgis.homework.telia.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lt.mazgis.homework.telia.lb.LBFxRatesClient;
import lt.mazgis.homework.telia.lb.fxrates.ext.CurrencyType;
import lt.mazgis.homework.telia.lb.fxrates.ext.FxRate;
import lt.mazgis.homework.telia.lb.fxrates.ext.FxRatesResponce;
import lt.mazgis.homework.telia.rest.dao.CurrencyRateDto;
import lt.mazgis.homework.telia.rest.dao.NoCurrencyListException;
import lt.mazgis.homework.telia.rest.dao.NoFxRatesException;

@RestController
public class CurrencyRatesREST {

    @Autowired
    private LBFxRatesClient lbFxRatesClient;

    @RequestMapping("/rates")
    public Collection<CurrencyRateDto> rates() {
	FxRatesResponce responce = this.lbFxRatesClient.getCurrencyList();
	final Collection<CurrencyType> currencyTypes = responce.getCurrenctyTypes();
	if (currencyTypes == null) {
	    throw new NoCurrencyListException();
	}
	final Map<String, CurrencyType> mappedCurrencies = new HashMap<>(responce.getCurrenctyTypes().size());
	currencyTypes.forEach(c -> mappedCurrencies.put(c.getAcronym(), c));
	responce = this.lbFxRatesClient.getCurrentFxRates("EU");
	final Collection<FxRate> rates = responce.getFxRates();
	if (rates == null) {
	    throw new NoFxRatesException();
	}
	final Collection<CurrencyRateDto> result = new ArrayList<>(rates.size());
	rates.forEach(r -> result.add(CurrencyRateDto.create(r, mappedCurrencies)));
	return result;
    }

    public LBFxRatesClient getLbFxRatesClient() {
	return this.lbFxRatesClient;
    }

    public void setLbFxRatesClient(final LBFxRatesClient lbFxRatesClient) {
	this.lbFxRatesClient = lbFxRatesClient;
    }

}
