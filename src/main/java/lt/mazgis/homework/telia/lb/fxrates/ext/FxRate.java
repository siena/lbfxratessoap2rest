package lt.mazgis.homework.telia.lb.fxrates.ext;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "FxRate")
public class FxRate {
    @XmlElement(name="Tp")
    private String type;
    @XmlElement(name="Dt")
    private String date;

    @XmlElement(name = "CcyAmt")
    private Collection<CurrencyAmount> currancyAmounts;

    public FxRate() {
    }

    public FxRate(final String type, final String date, final Collection<CurrencyAmount> currancyAmounts) {
	super();
	this.type = type;
	this.date = date;
	this.currancyAmounts = currancyAmounts;
    }

    public String getType() {
	return this.type;
    }
    public String getDate() {
	return this.date;
    }

    public Collection<CurrencyAmount> getCurrancyAmounts() {
	return this.currancyAmounts;
    }

}
