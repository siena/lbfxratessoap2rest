# LBFxRatesSOAP2REST

Lithuanian Bank currency rates SOAP service bridge to REST Service

To compile run command:
	
	mvn clean package -Dhttp.agent=any

After procject successfully build to start application run command:

	java -jar target/LB-SOAP2REST-0.0.1-SNAPSHOT.jar

REST API usage 
	
	curl http://localhost:8080/rates
	
Or run in your browser [http://localhost:8080/rates](http://localhost:8080/rates "http://localhost:8080/rates")  